// cars years less than 2000

let yearsFunction=require('./problem4.cjs');

function findingOldCars(carsArray){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
        
    }else if(Array.isArray(carsArray) && carsArray.length!==0){
        let allYears=yearsFunction(carsArray);
        let older=[];
        for (let index=0;index<allYears.length;index++){
            if(allYears[index]<2000){
                older.push(allYears[index]);
            }
        }
        return older;
    }else{
        return [];
    }
}

module.exports = findingOldCars;



    