// Finding last car in given array

function findLastCar(carsArray){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
    }else if(Array.isArray(carsArray) && carsArray.length!==0){
        let lastCar=carsArray[(carsArray.length)-1];
        return lastCar;
    }else{
        return [];
    }
}

module.exports = findLastCar