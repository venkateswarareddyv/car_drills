// Finding a car based on their Id;

function findInformationOfacar(carsArray,carsId){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
    }
    if(carsId===undefined){
        return [];
    }
    if (Array.isArray(carsArray) && carsArray.length!==0 && carsId!==undefined){
        for (let index=0;index<carsArray.length;index++){
            if(carsArray[index].id===carsId){
                return carsArray[index];
            }
        }
        return [];
    }else{
        return [];
    }
}

module.exports=findInformationOfacar;

// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"