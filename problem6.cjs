// AUDI and BMW

function findAudiAndBmw(carsArray){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
    }
    else if(Array.isArray(carsArray) && carsArray.length!==0){
        let audiAndBmw=[];
        for (let index=0;index<carsArray.length;index++){
            if(carsArray[index].car_make === "Audi" || carsArray[index].car_make === "BMW" ){
                audiAndBmw.push(carsArray[index]);
            }
        }
        return audiAndBmw;
    }else{

    }
}

module.exports = findAudiAndBmw;
