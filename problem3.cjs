// sorting based on alphabetical order

function sortCars(carsArray){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
    }else if(Array.isArray(carsArray) && carsArray.length!==0){
        let cars_model=[];
        for (let index=0;index<carsArray.length;index++){
            cars_model.push(carsArray[index].car_model);
        }
        return cars_model.sort()
    }else{
        return [];
    }
    
}

module.exports = sortCars;

