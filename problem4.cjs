// Cars year present in the lot

function findYearOfCar(carsArray){
    if(carsArray===undefined ||carsArray.length===0){
        return [];
    }
    else if(Array.isArray(carsArray) && carsArray.length!==0){
        let cars_years=[];
        for (let index=0;index<carsArray.length;index++){
            cars_years.push(carsArray[index].car_year);
        }
        return cars_years;
    }else{
        return [];
    }
}

module.exports = findYearOfCar;
